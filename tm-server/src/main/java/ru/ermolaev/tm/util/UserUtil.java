package ru.ermolaev.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.ermolaev.tm.dto.AuthUser;
import ru.ermolaev.tm.exception.user.AccessDeniedException;

public class UserUtil {

    static public AuthUser getAuthUser() throws AccessDeniedException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) throw new AccessDeniedException();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof AuthUser)) throw new AccessDeniedException();
        return (AuthUser) principal;
    }

    static public String getAuthUserId() throws AccessDeniedException {
        return getAuthUser().getUserId();
    }

}
