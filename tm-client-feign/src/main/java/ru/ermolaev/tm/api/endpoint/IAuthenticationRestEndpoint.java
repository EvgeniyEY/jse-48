package ru.ermolaev.tm.api.endpoint;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "IAuthenticationRestEndpoint", name = "tm-server")
@RequestMapping("/api/rest/authentication")
public interface IAuthenticationRestEndpoint {

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean login(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    );

    @GetMapping(value = "/logout")
    void logout();

}
