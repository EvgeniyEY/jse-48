package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.IProjectRestEndpoint;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    protected IProjectRestEndpoint projectEndpoint;

    @Autowired
    public AbstractProjectListener(
            @NotNull final IProjectRestEndpoint projectEndpoint
    ) {
        this.projectEndpoint = projectEndpoint;
    }

}
