package ru.ermolaev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.IAuthenticationRestEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class LoginListener extends AbstractListener {

    private final IAuthenticationRestEndpoint authenticationEndpoint;

    @Autowired
    public LoginListener(
            @NotNull final IAuthenticationRestEndpoint authenticationEndpoint
    ) {
        this.authenticationEndpoint = authenticationEndpoint;
    }

    @NotNull
    @Override
    public String command() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account.";
    }

    @Override
    @EventListener(condition = "@loginListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        authenticationEndpoint.login(login, password);
        System.out.println("[OK]");
    }

}
