package ru.ermolaev.tm.listener;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.event.ConsoleEvent;

public abstract class AbstractListener {

    @Nullable
    public abstract String command();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void handler(final ConsoleEvent event) throws Exception;

}
